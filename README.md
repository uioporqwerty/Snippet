# Snippet
Snippet is a text sharing application where you control the file permissions. The application is written for blockstack users and using Gaia Storage which means you control the data. It is a decentralized and serverless application that allows you to share snippets with other blockstack users, publically or privately.

# Requirements
You must have a Blockstack account. You'll be able to sign-up for one once you login.

# Installation
1. Clone the repository. 
2. `npm install`
3. `npm run start`