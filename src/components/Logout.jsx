// @flow
import React, {Component} from 'react'
import Button from '@material-ui/core/Button'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import Dialog from '@material-ui/core/Dialog'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import ExitToAppRounded from '@material-ui/icons/ExitToAppRounded'

import AuthorizationService from '../api/Authorization'
import BlockstackAuthorizationService from '../api/BlockstackAuthorization'

type Props = {}
type State = {
    open: boolean
}

export default class Logout extends Component<Props, State> {
    state = {
      open: false
    }

    _authorizationService : AuthorizationService = new BlockstackAuthorizationService()

    openLogoutConfirmationModal = () => {
      this.setState({ open: true })
    }

    handleLogoutResponse = () => {
      this._authorizationService.signout()
    }

    handleCancelLogoutConfirmationResponse = () => {
      this.setState({ open: false })
    }

    render () {
      return (
        <div>
          <Tooltip title="Log Out">
            <IconButton aria-label="Log Out" onClick={ this.openLogoutConfirmationModal }>
              <ExitToAppRounded />
            </IconButton>
          </Tooltip>
          <Dialog
            disableBackdropClick={true}
            disableEscapeKeyDown={true}
            open={this.state.open}
            aria-labelledby="logout-confirmation-title"
            aria-describedby="logout-confirmation-description"
          >
            <DialogTitle id="logout-confirmation-title">Are you sure you want to log out?</DialogTitle>
            <DialogContent>
              <DialogContentText id="logout-confirmation-description">
                            Would you like to log out of Snippet? Any text in the editor will not be saved prior to logging out.
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button onClick={ this.handleCancelLogoutConfirmationResponse } color="primary">
                            Cancel
              </Button>
              <Button onClick={ this.handleLogoutResponse } color="primary">
                            Log out
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      )
    }
}
