// @flow
import React, {Component} from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import {
  isSignInPending,
  isUserSignedIn,
  redirectToSignIn,
  handlePendingSignIn
} from 'blockstack/lib'
import Editor from './Editor'
import Signin from './Signin'

type Props = {}

const theme = createMuiTheme({})

const styles = theme => ({})

class App extends Component<Props> {
  handleSignIn (e: any) {
    e.preventDefault()
    redirectToSignIn()
  }

  render () {
    return (
      <MuiThemeProvider theme={theme}>
        {
          console.log(isUserSignedIn())

        }
        {

          isUserSignedIn()
            ? <Editor />
            : <Signin handleSignIn={ this.handleSignIn } />
        }
      </MuiThemeProvider>
    )
  }

  componentWillMount () {
    if (isSignInPending()) {
      handlePendingSignIn().then((userData) => {
        window.location = window.location.origin
      })
    }
  }
}

export default withStyles(styles, { withTheme: true })(App)
