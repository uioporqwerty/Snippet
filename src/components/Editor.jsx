// @flow
import React, {Component} from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Tooltip from '@material-ui/core/Tooltip'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import IconButton from '@material-ui/core/IconButton'
import Grid from '@material-ui/core/Grid'
import {withStyles} from '@material-ui/core/styles'
import SettingsRounded from '@material-ui/icons/SettingsRounded'
import ExitToAppRounded from '@material-ui/icons/ExitToAppRounded'
import CodeRounded from '@material-ui/icons/CodeRounded'
import AceEditor from 'react-ace'
import {
  isSignInPending
} from 'blockstack/lib'

import Logout from './Logout.jsx'
import EditorSettings from './EditorSettings.jsx'
import FileProvider from '../api/FileProvider'
import BlockstackFileProvider from '../api/BlockstackFileProvider'
import File from '../models/File'

type Props = {
  classes: Object
}

type State = {
  theme: string,
  fontSize: number,
  language: string,
  file: File
}

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  flex: {
    flexGrow: 1
  }
})

class Editor extends Component<Props, State> {
  state = {
    theme: 'monokai',
    language: 'plain_text',
    fontSize: 12,
    file: new File()
  }

  constructor (props : any) {
    super(props)

    this.fileProvider = new BlockstackFileProvider()

    console.log(`Loading brace/theme/${this.state.theme}`)
    require(`brace/theme/${this.state.theme}`)

    console.log(`Loading brace/mode/${this.state.language}`)
    require(`brace/mode/${this.state.language}`)
  }

  adjustFontSize = (event: any) => {
    console.log(`Adjusting font size from ${this.state.fontSize} to ${event.target.value}`)
    this.setState({
      fontSize: event.target.value
    })
  }

  adjustLanguage = (event: any) => {
    const newLanguage = event.target.value
    import(
      /* webpackMode: "lazy-once", webpackChunkName: "modes" */
      `brace/mode/${newLanguage}.js`).then((success) => {
      this.setState({
        language: newLanguage
      })
    },
    (error) => {
      console.error(error)
    }
    )
  }

  adjustTheme = (event: any) => {
    const newTheme = event.target.value
    import(
      /* webpackMode: "lazy-once", webpackChunkName: "themes" */
      `brace/theme/${newTheme}.js`).then((success) => {
      this.setState({
        theme: newTheme
      })
    },
    (error) => {
      console.error(error)
    }
    )
  }

  saveFile = () => {
    try {
      this.fileProvider.save(this.state.file).then(
        (success) => {
          console.log('stored file')
        }
      )
    } catch (error) {
      console.error(error)
    }
  }

  onEditorChange = (value: string) => {
    this.setState(previousState => ({
      file: {
        ...previousState.file,
        content: value
      }
    }))
  }

  render () {
    const { classes } = this.props

    return (
      !isSignInPending()
        ? <div style={{overflowX: 'hidden'}}>
          <div className={classes.root}>
            <AppBar position="static">
              <Toolbar>
                <Typography variant="title"
                  color="inherit"
                  className={classes.flex}>
                  Snippet
                </Typography>

                <Tooltip title="Gitlab">
                  <IconButton aria-label="Gitlab"
                    target="_blank"
                    href="https://gitlab.com/uioporqwerty/Snippet">
                    <CodeRounded />
                  </IconButton>
                </Tooltip>

                <Logout />

              </Toolbar>
            </AppBar>
          </div>
          <Grid container spacing={16} className={classes.root} style={{marginTop: 20}}>
            <Grid item xs={12}>
              <EditorSettings
                fontSize={this.state.fontSize}
                adjustFontSize={this.adjustFontSize}
                language={this.state.language}
                adjustLanguage={this.adjustLanguage}
                theme={this.state.theme}
                adjustTheme={this.adjustTheme}
                saveFile={this.saveFile}
              />
              <AceEditor
                theme={this.state.theme}
                mode={this.state.language}
                wrapEnabled
                focus
                showPrintMargin={false}
                fontSize={this.state.fontSize}
                editorProps={{
                  $blockScrolling: Infinity
                }}
                width="100%"
                onChange={this.onEditorChange}
                value={this.state.file.content}
              />
            </Grid>
          </Grid>
        </div>
        : null
    )
  }
}

export default withStyles(styles)(Editor)
