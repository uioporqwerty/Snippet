// @flow
import React, {Component} from 'react'
import {withStyles} from '@material-ui/core/styles'
import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import MenuItem from '@material-ui/core/MenuItem'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import Popover from '@material-ui/core/Popover'
import IconButton from '@material-ui/core/IconButton'
import Tooltip from '@material-ui/core/Tooltip'
import SaveOutlined from '@material-ui/icons/SaveOutlined'

import EditorConfigurationProvider from '../api/EditorConfigurationProvider'

type Props = {
    fontSize: number,
    adjustFontSize: Function,
    language: string,
    adjustLanguage: Function,
    theme: string,
    adjustTheme: Function,
    saveFile: Function,
    classes: Object,
}

type State = {
    anchorEl: any
};

const styles = theme => ({
  formControl: {
    minWidth: 120
  },
  editorToolbar: {
    marginLeft: 41
  },
  toolbarIcon: {
    fontSize: 36
  },
  toolbarIconContainer: {
    marginLeft: 10
  }
})

class EditorSettings extends Component<Props, State> {
    static defaultProps = {
      fontSize: 12
    }

    state = {
      anchorEl: null
    }

    editorConfigurationProvider:EditorConfigurationProvider = new EditorConfigurationProvider();

    showEditorSettingsPopover = (event: any) => {
      this.setState({
        anchorEl: event.currentTarget
      })
    }

    closeEditorSettingsPopover = () => {
      this.setState({
        anchorEl: null
      })
    }

    render () {
      const { classes } = this.props

      return (
        <div className={classes.editorToolbar}>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="font-size">Font Size</InputLabel>
            <Select
              value={this.props.fontSize}
              onChange={this.props.adjustFontSize}
              id="font-size" >
              {
                this.editorConfigurationProvider.getFontSizes().map((item: number, index: number) =>
                  <MenuItem key={index} value={item}>{item}</MenuItem>
                )
              }
            </Select>
          </FormControl>

          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="editor-language">Language</InputLabel>
            <Select
              value={this.props.language}
              onChange={this.props.adjustLanguage}
              id="editor-language">
              {
                this.editorConfigurationProvider.getLanguages().map((item: string, index: number) =>
                  <MenuItem key={index} value={item}>{item}</MenuItem>
                )
              }
            </Select>
          </FormControl>

          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="editor-theme">Theme</InputLabel>
            <Select
              value={this.props.theme}
              onChange={this.props.adjustTheme}
              id="editor-theme">
              {
                this.editorConfigurationProvider.getThemes().map((item: string, index: number) =>
                  <MenuItem key={index} value={item}>{item}</MenuItem>
                )
              }
            </Select>
          </FormControl>

          <IconButton aria-label="Save"
            className={classes.toolbarIconContainer}
            onClick={this.props.saveFile}>
            <SaveOutlined className={classes.toolbarIcon}/>
          </IconButton>

        </div>
      )
    }
}

export default withStyles(styles)(EditorSettings)
