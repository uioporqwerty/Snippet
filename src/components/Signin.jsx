// @flow
import React, {Component} from 'react'
import Button from '@material-ui/core/Button'

type Props = {
  handleSignIn: Function
}
type State = {}

export default class Signin extends Component<Props, State> {
  render () {
    return (
      <div className="sign-in">
        <p>
          <Button
            variant="contained"
            color="primary"
            onClick={ this.props.handleSignIn.bind(this) }>
              Log In with Blockstack
          </Button>
        </p>
      </div>
    )
  }
}
