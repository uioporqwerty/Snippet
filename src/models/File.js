import StringUtilities from '../utilities/StringUtilities'

// @flow
export default class File {
    content: String;
    name: String;

    constructor () {
      this.name = StringUtilities.getUniqueString()
    }
}
