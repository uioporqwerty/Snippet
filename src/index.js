// @flow
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import App from './components/App.jsx'
import styles from './styles/style.scss'

const root = document.getElementById('root')

if (root) {
  ReactDOM.render(<App />, root)
}
