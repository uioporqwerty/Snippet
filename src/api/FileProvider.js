// @flow
import File from '../models/File'

interface FileProvider {
    save(file: File): Promise<any>;
}
