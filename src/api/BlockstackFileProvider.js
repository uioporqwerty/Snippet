// @flow
import {
  putFile
} from 'blockstack/lib'
import FileProvider from './FileProvider'
import File from '../models/File'

export default class BlockstackFileProvider implements FileProvider {
    save = (file: File): Promise<any> => {
      console.log(`Saving file`)
      console.log(file)
      return putFile(`${file.name}.json`, file)
    }
}
