// @flow
import { signUserOut } from 'blockstack/lib'
import Authorization from './Authorization'

export default class BlockstackAuthorization implements Authorization {
    signout = () => {
      signUserOut(window.location.origin)
    }
}
