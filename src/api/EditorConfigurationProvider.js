// @flow
import configuration from '../editor-configuration.json'

export default class EditorConfigurationProvider {
    getFontSizes = () => {
      return configuration.fontSizes
    }

    getLanguages = () => {
      return configuration.languages
    }

    getThemes = () => {
      return configuration.themes
    }
}
