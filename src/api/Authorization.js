// @flow
interface Authorization {
    signout(): void
}
