const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const Uglify = require('uglifyjs-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = merge(common, {
  mode: 'production',
  plugins: [
    new Uglify(),
    new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      reportFileName: 'blockbin-bundle-analysis.html'
    })
  ]
})
